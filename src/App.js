import React, { useState } from "react";
import { Form, Button, Container } from "react-bootstrap";
import "./App.css";

function App() {
  const [answer, setAnswer] = useState(0);
  const [input1, setInput1] = useState(0);
  const [input2, setInput2] = useState(0);

  const add = (field1, field2) => {
    setAnswer(Number(field1) + Number(field2));
  };
  const subtract = (field1, field2) => {
    setAnswer(Number(field1) - Number(field2));
  };
  const multiply = (field1, field2) => {
    setAnswer(Number(field1) * Number(field2));
  };
  const divide = (field1, field2) => {
    setAnswer(Number(field1) / Number(field2));
  };
  const reset = () => {
    setAnswer(0);
    setInput1(0);
    setInput2(0);
  };

  return (
    <Container className="calc-container">
      <h1>Calculator</h1>
      <h2>{answer}</h2>

      <Form.Group className="mb-3 p-3" controlId="input1">
        <Form.Control
          type="number"
          placeholder="0"
          value={input1}
          onChange={(e) => {
            setInput1(e.target.value);
          }}
          width="50%"
          required
        />
      </Form.Group>

      <Form.Group className="mb-3 p-3" controlId="input2">
        <Form.Control
          type="number"
          placeholder="0"
          value={input2}
          onChange={(e) => {
            setInput2(e.target.value);
          }}
          width="50%"
          required
        />
      </Form.Group>

      <div>
        <Button className="btn btn-style" onClick={() => add(input1, input2)}>
          Add
        </Button>
        <Button className="btn-style" onClick={() => subtract(input1, input2)}>
          Subtract
        </Button>
        <Button className="btn-style" onClick={() => multiply(input1, input2)}>
          Multiply
        </Button>
        <Button className="btn-style" onClick={() => divide(input1, input2)}>
          Divide
        </Button>
        <Button className="btn-style" onClick={() => reset()}>
          Reset
        </Button>
      </div>
    </Container>
  );
}

export default App;
